package com.gl.miniproject2.entity;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
//Lombok
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
@Data
@Entity
@Table(name = "mycart")
public class MyCart {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long Itemid;

	private String itemName;
	private int price;
}
