<%@taglib uri = "http://www.springframework.org/tags/form"
 prefix = "form"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
		<div align="center">
		<h2>Enter AuthorName to get the Information</h2>
		<form:form method="POST" action="afinfByAuthorName" modelAttribute="book">
			<table>
				<tr>
					<td><form:label path="authorname">AuthorName</form:label></td>
					<td><form:input path="authorname" /></td>
				</tr>

				<tr>
					<td colspan="2"><input type="submit" value="GetAuthorNameDetails" /></td>
				</tr>
			</table>
		</form:form>
	</div>
</body>
</html>