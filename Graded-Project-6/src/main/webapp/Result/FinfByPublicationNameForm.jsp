<%@taglib uri = "http://www.springframework.org/tags/form"
 prefix = "form"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
		<div align="center">
		<h2>Enter PublishName to get the Information</h2>
		<form:form method="POST" action="/finfByPublicationName" modelAttribute="book">
			<table>
				<tr>
					<td><form:label path="publicationname">PublicationName</form:label></td>
					<td><form:input path="publicationname" /></td>
				</tr>

				<tr>
					<td colspan="2"><input type="submit" value="GetPublicationNameDetails" /></td>
				</tr>
			</table>
		</form:form>
	</div>
</body>
</html>