package com.gl.Graded.Project6.model;

import java.util.List;

import javax.persistence.*;

@Entity
@Table
public class Book {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="BookId")
	private int bookId;
	private String bookName;
	private String authorname;
	private String description;
	private String genre;
	private Integer noOfCopiesSold;
	private Double price;
	private String titlename;
	private String publicationname;
	
	public Book() {
		
	}
	
	
	public Book(String bookName, String authorname, String description, String genre, Integer noOfCopiesSold,
			Double price, String titlename, String publicationname, int bookId) {
		super();
		this.bookName = bookName;
		this.authorname = authorname;
		this.description = description;
		this.genre = genre;
		this.noOfCopiesSold = noOfCopiesSold;
		this.price = price;
		this.titlename = titlename;
		this.publicationname = publicationname;
		this.bookId = bookId;
	}
	
	public int getBookId() {
		return bookId;
	}
	public void setBookId(int bookId) {
		this.bookId = bookId;
	}
	public String getBookName() {
		return bookName;
	}
	public void setBookName(String bookName) {
		this.bookName = bookName;
	}
	public String getAuthorname() {
		return authorname;
	}
	public void setAuthorname(String authorname) {
		this.authorname = authorname;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public Integer getNoOfCopiesSold() {
		return noOfCopiesSold;
	}
	public void setNoOfCopiesSold(Integer noOfCopiesSold) {
		this.noOfCopiesSold = noOfCopiesSold;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public String getTitlename() {
		return titlename;
	}
	public void setTitlename(String titlename) {
		this.titlename = titlename;
	}
	public String getPublicationname() {
		return publicationname;
	}
	public void setPublicationname(String publicationname) {
		this.publicationname = publicationname;
	}

	
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="bookid", referencedColumnName = "bookId")
	private List<AddToFavourite> favlist;


	public List<AddToFavourite> getFavlist() {
		return favlist;
	}


	public void setFavlist(List<AddToFavourite> favlist) {
		this.favlist = favlist;
	}
	
}



/*
insert into book values(1,"James J. Gosling","Java","analysis of the language","BioGraphy",5,1000,"ENADU","JAVA");

insert into book values(2,"Dennis_Ritchi","c","c_language","Biography",15,500,"SAKSHI","First programming language");

insert into book values(3,"Bimal jalal","The India Story","The history of india","Biography",10,600,"ABN","The India story");

insert into book values(4,"A P j Abdul Kalam"," Wings of abdul kalam","His life story","Autobiography",20,2000,"ENADU","ABDUL Kalam");

insert into book values(5,"Atmakatha","Anna Chandy","se Life Story","Autobiography",5,350,"ABN","Short Stroy");
*/