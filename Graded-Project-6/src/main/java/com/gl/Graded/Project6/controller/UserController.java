package com.gl.Graded.Project6.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.gl.Graded.Project6.model.Book;
import com.gl.Graded.Project6.model.User;
import com.gl.Graded.Project6.repositories.UserRepositories;
import com.gl.Graded.Project6.service.BookService;

import java.util.List;

import javax.servlet.http.HttpSession;

@Controller // annotation that is itself annotated with @Controller and @ResponseBody
public class UserController {
	@Autowired // getting object of UserRepositories
	UserRepositories userRep;

	@Autowired // getting object of bookService
	BookService bookService;

	// mapping to get welcome page
	@GetMapping("/")
	public String getwelcomePage() {
		return "welcome";
	}

	// mapping to get UserDashBoard
	@RequestMapping("/dashBoard")
	public ModelAndView getUserDashBoard(HttpSession session,String msg) {
		List<Book> bookList = bookService.getAllBooks();
		return new ModelAndView("UserDashBoard", "bookList", bookList);

	}

	// mapping to get LoginPage
	@RequestMapping("/loginPage")
	public ModelAndView getUserLoginPage(@ModelAttribute("LoginErrorMessage") String errorMessage) {
		ModelAndView modelAndView = new ModelAndView("userLoginPage", "loginData", new User());
		if (errorMessage == null) {

			return modelAndView;
		} else {
			modelAndView.addObject("LoginErrorMessage", errorMessage);
			return modelAndView;
		}
	}

	// mapping to validate user
	@RequestMapping("/validateUserLogin")
	public ModelAndView validateUser(@ModelAttribute("loginData") User u,String errorMessage,HttpSession session)throws Exception {

		java.util.Optional<User> uList = userRep.userValidation(u.getEmail(), u.getPassword());

		System.out.println("--------");

		User obj = uList.get();
//		session.setAttribute("id", obj.getUserId());
//		session.setAttribute("email", obj.getEmail());
//		session.setAttribute("password", obj.getPassword());
		session.setAttribute("user", obj);

		System.out.println(obj.getUserId());
		System.out.println(obj.getEmail());
		System.out.println(obj.getPassword());

		User uFind = null;
		if (uList.isPresent()) {

			System.out.println("user found");
			uFind = uList.get();
			return new ModelAndView("redirect:/dashBoard");

		} else {
			System.out.println("User not found");
			return new ModelAndView("userLoginPage", "LoginErrorMessage", "Invalid username or password");
		}

	}

	// mapping to get Registration_Form
	@GetMapping("/registration")
	public ModelAndView getRegistration_Form() {

		return new ModelAndView("userRegisterPage", "registerData", new User());
	}

	// mapping to register
	@PostMapping("/Register")
	public ModelAndView saveRegistrtionData(@ModelAttribute("registerData") User u) {
		userRep.save(u);
		return new ModelAndView("redirect:/loginPage");
	}

	// mapping to logout
	@RequestMapping("/logout")
	public String logout(HttpSession session) {
		session.invalidate();
		return "redirect:/loginPage";
	}

	// mapping to get back to user dashBoard
	@RequestMapping("/backTotoUserDashBoard")
	public String backTotoUserDashBoard() {
		return "redirect:/dashBoard";
	}
}
