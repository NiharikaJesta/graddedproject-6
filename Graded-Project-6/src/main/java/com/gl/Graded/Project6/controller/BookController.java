package com.gl.Graded.Project6.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.gl.Graded.Project6.model.AddToFavourite;
import com.gl.Graded.Project6.model.Book;
import com.gl.Graded.Project6.model.User;
import com.gl.Graded.Project6.repositories.AddToFavouriteRepositories;
import com.gl.Graded.Project6.repositories.BookRepositories;
import com.gl.Graded.Project6.service.BookService;

@RestController // annotation that is itself annotated with @Controller and @ResponseBody
public class BookController {

	@Autowired // getting object of bookService
	BookService bookService;

	@Autowired // getting object of BookRepositories
	BookRepositories bookRep;

	@Autowired // getting object of AddToFavouriteRepositories
	AddToFavouriteRepositories favRep;

	// mapping to get book dashBoard
	@RequestMapping("/bookDashBoard")
	public ModelAndView getBookDashBoard(HttpSession session) {
		User user = (User) session.getAttribute("user");
		List<Book> bookList = bookService.getAllBooks();
		return new ModelAndView("bookDashBoard", "bookList", bookList);
	}

	// mapping to get book form
	@RequestMapping("/bookForm")
	public ModelAndView getBookForm() {
		return new ModelAndView("bookForm", "bookData", new Book());
	}

	// mapping to save book
	@RequestMapping("/saveBook")
	public String saveBook(@ModelAttribute("bookData") Book book) {
		bookService.saveBook(book);
		return "redirect:/bookDashBoard";
	}

	// mapping to delete book
	@RequestMapping("/deleteBook/{bookId}")
	public ModelAndView deleteBook(@PathVariable("bookId") int id) {
		bookService.deleteBook(id);
		return new ModelAndView("redirect:/bookDashBoard");
	}

	// mapping to find by id
	@RequestMapping("/updateBook/{bookId}")
	public ModelAndView updateBook(@PathVariable("bookId") int id) {
		Book book = bookService.getBookById(id);
		return new ModelAndView("bookUpdateForm", "ubookData", book);
	}

	// mapping for update book
	@RequestMapping("/updated")
	public ModelAndView updated(@ModelAttribute("ubookData") Book book) {
		bookService.saveBook(book);
		return new ModelAndView("redirect:/bookDashBoard");
	}

	//// mapping to add favorite book
	@RequestMapping("/addToFavorite/{bookId}")
	public ModelAndView saveFavorite(@PathVariable("bookId") int id, String msg,HttpSession session) {
		Book book = bookService.getBookById(id);
		User user = (User) session.getAttribute("user");
		System.out.println("id-->" + user.getUserId());
		AddToFavourite fav = new AddToFavourite();

		fav.setBook(book);
		fav.setUser(user);
		favRep.save(fav);
		AddToFavourite a = favRep.save(fav);
		return new ModelAndView("redirect:/dashBoard", "msg", "Book Saved Successfully");

	}

	// mapping to retrieve favorite book
	@RequestMapping("/getfavouriteBooks")
	public ModelAndView getfavouriteBooks(HttpSession session) {
		User user = (User) session.getAttribute("user");
		List<AddToFavourite> favList = favRep.getBooks(user);
		return new ModelAndView("AddToFavourite", "favList", favList);
	}
	
	
	
	
	
	
	
	
	

	//// mapping to searchByIdForm
	@RequestMapping("/searchByIdForm")
	public ModelAndView searchByIdForm() {
		return new ModelAndView("FindBookByIdForm", "book", new Book());
	}

	// Admin can search new book (by id, by author
	@RequestMapping("/searchById")
	public ModelAndView searchByIdOrAuthor(@ModelAttribute("book") Book bk) {
		Book b = bookService.getBookById(bk.getBookId());
		return new ModelAndView("sucess", "bookData", b);
	}

	// mapping to finfByAuthorNameForm
	@RequestMapping("/finfByAuthorNameForm")
	public ModelAndView finfByAuthorForm() {
		return new ModelAndView("FindBookByAuthorName", "book", new Book());

	}

	// User can search book by Author name
	@RequestMapping("/finfByAuthorName")
	public ModelAndView finfByAuthorName(@ModelAttribute("book") Book bk) {
		Book b = bookRep.findByAuthorname(bk.getAuthorname());
		return new ModelAndView("sucess", "bookData", b);
	}

	// mapping to finfByTitleNameForm
	@RequestMapping("/finfByTitleNameForm")
	public ModelAndView finfByTitleNameForm() {
		return new ModelAndView("FinfByTitleNameForm", "book", new Book());
	}

	// mapping to finfByTitleName
	@RequestMapping("/finfByTitleName")
	public ModelAndView finfByTitleName(@ModelAttribute("book") Book bk) {
		Book b = bookRep.findByTitlename(bk.getTitlename());
		return new ModelAndView("sucess", "bookData", b);
	}

	// mapping to finfBy Publication Name Form
	@RequestMapping("/finfByPublicationNameForm")
	public ModelAndView finfByPublicationNameForm() {
		return new ModelAndView("FinfByPublicationNameForm", "book", new Book());
	}

	// mapping to findByBookName
	@RequestMapping("/finfByPublicationName")
	public ModelAndView finfByPublicationName(@ModelAttribute("book") Book bk) {
		List<Book> b = bookRep.findByPublicationname(bk.getPublicationname());
		return new ModelAndView("PublicationDeatils", "bookData", b);
	}

	

	// mapping to findyPriceRange
	@RequestMapping("/findyPriceRange")
	public ModelAndView findyPriceRange() {
		List<Book> b = bookRep.pricerange(100, 10000);
		for(Book book:b){
			System.out.println("Book Name"+book.getBookName());
			System.out.println("Book price"+book.getPrice());
		}
		return new ModelAndView("price range","bookData",b);
	}

	// mapping to sortByPrize
	@RequestMapping("/sortByPrize")
	public ModelAndView sortByPrize() {
		List<Book> b = bookRep.findByOrderByPriceAsc();
		return new ModelAndView("sortByPrize", "bookData", b);
	}

}
